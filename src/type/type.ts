import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export default interface movietype{
    name: string
    date: string
    time: string
    movieType: number
    seat: string
}
export const useTypeStore = defineStore('type', () => {
    const items = ref<movietype[]>([
        {name: 'Transformers: rise of the beasts',date: 'unknown', time: '10:00', movieType: 1, seat: ''},
        {name: 'The Super Mario Bros. Movie',date: 'unknown', time: '10:00', movieType: 2, seat: ''},
        {name: "Doraemon Movie: Nobita's Mermaid Legend",date: 'unknown', time: '10:00', movieType: 3, seat: ''},
        {name: 'Spider man: across the spider-verse',date: 'unknown' ,time: '10:00', movieType: 4, seat: ''},
        {name: 'The Flash (2023)',date: 'unknown', time: '10:00', movieType: 5, seat: ''},
]);

function getMovieType(movieType: number) {
    return items.value.filter((item) => item.movieType === movieType)
}

  
    return {  items, getMovieType}
  })
  
  


